'use strict'

const path = require('path')
const setting = require('../src/config/setting')


module.exports = {

  dev: {
    // Paths
    assetsSubDirectory: 'static',
    // 静态资源加前缀?
    assetsPublicPath: '/',
    proxyTable: {
      // 设置图片代理,对资源 asset 的进行代理,对资源的代码，必须设置这个代理，因为前端使用的端口和后端的不一致，导致会有跨域，需要走一次代理来访问后端的文件，无法去掉
      '/asset': {
        // 图片地址
        target: setting.resourceURL,
        // true开启跨域
        changeOrigin: true,
        secure:false,
        pathRewrite: {
          '^/asset': ''
        }
      },
    //   // socket请求地址
    //   '/socket': {
    //     // target:'ws://127.0.0.1:9001/wsc/',
    //     target: setting.socketURL,
    //     ws: true,
    //     changeOrigin: true,
    //     pathRewrite: {
    //       '^/socket': ''
    //     }
    // }
    },

    // Various Dev Server settings
    // host: 'localhost', // 默认是 localhost，只能使用这个访问，设置为当前就可以全部都访问了，否则使用 0.0.0.0 则可以其他也进行远程访问
    host: setting.host, // 默认是 localhost，只能使用这个访问，设置为当前就可以全部都访问了，否则使用 0.0.0.0 则可以其他也进行远程访问
    // port: 8083, // 项目访问端口
    port: setting.port, // 项目访问端口
    autoOpenBrowser: false,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-


    /**
     * Source Maps
     */

    // https://webpack.js.org/configuration/devtool/#development
    devtool: 'cheap-module-eval-source-map',

    // If you have problems debugging vue-files in devtools,
    // set this to false - it *may* help
    // https://vue-loader.vuejs.org/en/options.html#cachebusting
    cacheBusting: true,

    cssSourceMap: true
  },

  build: {
    // Template for index.html
    index: path.resolve(__dirname, '../dist/index.html'),

    // Paths
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    // 静态资源加前缀?
    assetsPublicPath: '/',

    /**
     * Source Maps
     */

    productionSourceMap: true,
    // https://webpack.js.org/configuration/devtool/#production
    devtool: '#source-map',

    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],

    // Run the build command with an extra argument to
    // View the bundle analyzer report after build finishes:
    // `npm run build --report`
    // Set to `true` or `false` to always turn it on or off
    bundleAnalyzerReport: process.env.npm_config_report
  }
}
