import Vue from 'vue';
import Vuex from 'vuex';

// 这是vuex的文件夹，详细参考文件：https://www.cnblogs.com/lgnblog/p/16189919.html

Vue.use(Vuex)

let store = new Vuex.Store({
  /**
   * 状态数据容器
   * 这里放要存储的数据
   */
  state: {
    /// 访问数据的第一种方式，this.$store.state.全局数据名称  ，第二种 import { mapState } from 'vuex'
    // 导航信息
    navigationList: [],
  },
  /**
   * 用于改变store中的数据
   * 只能通过mutaion改变store数据、不可以直接操作store数据。触发的第一种方式，this.$store.commit('navigationListChange') 或 this.$store.commit('navigationListChange', 值)
   * 第二种方式，import {mapMutations } from "vuex";//从 vuex 中按需导入 mapMutations 函数    ...mapMutations(['navigationListChange']),
   */
  mutations: {
    /**
     * 导航信息变动
     * @param state 状态数据容器
     * @param navigationList 参数
     */
    navigationListChange(state, navigationList){
      state.navigationList = navigationList
    }
  },
  /**
   * 用于处理异步任务
   * 如果通过异步操作变更数据，必须通过 Action，而不能使用 Mutation，但是在 Action 中还是要通过触发 Mutation的方式间接变更数据。
   */
  actions: {
  },
  /**
   * Getter 用于对 Store 中的数据进行加工处理形成新的数据。
   * Getter 可以对 Store 中已有的数据加工处理之后形成新的数据，类似 Vue 的计算属性
   */
  getters:{
  }
});

export default store;
