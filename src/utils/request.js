/**
 * 封装 axios 的请求和返回
 */
import axios from 'axios'
// 使用element-ui Message做消息提醒
import { Message } from 'element-ui';
import qs from 'qs'
import router from "@/router";
import setting from '@/config/setting'
// import vuex from '../vuex'

// 1、--- 创建新的 axios 实例
const service = axios.create({
  // 请求后台地址， baseURL + 你写的请求地址
  // baseURL: "http://127.0.0.1:8084/ps/",
  baseURL: setting.baseURL,
  // 超时时间 单位是ms
  // timeout: 5000
  timeout: setting.timeout
})

// 定义一个对象用于存储 loginErr 状态，是否出现错误的状态
let loginErr = false;
// 用于跟踪当前正在进行的请求数量
let activeRequests = 0;

// 2、--- 请求拦截器。发请求前做的一些处理，数据转化，配置请求头，设置token,设置loading等，根据需求去添加
service.interceptors.request.use(config => {
  // 请求发起前增加正在进行的请求数量
  activeRequests ++;

  // 2.1 设置请求头
  if (config.existingFile){
    config.headers = {
      'Content-Type':'multipart/form-data'
    }
  } else {
    config.headers = {
      'Content-Type':'application/x-www-form-urlencoded'
    }
  }

  // 2.2 处理数据,文件提交的则不需要序列化
  if (config.existingFile){

  } else {
    //数据转化,也可以使用qs转换
    //sys.data = JSON.stringify(sys.data);
    config.data = qs.stringify(config.data)
  }

  // 2.3 读取token
  //注意使用token的时候需要引入cookie方法或者用本地localStorage等方法，推荐js-cookie
  let localStorage = window.localStorage;
  // 读取 token
  let token = localStorage.token;
  if (token){
    // 参数中携带token
    //sys.params = {'token':token}
    // 请求头中携带token
    config.headers.token = token;
  }

  return config;
}, error => {
  Promise.reject(error);
})

// 3、--- 响应拦截器
service.interceptors.response.use(response => {
  // 请求完成后减少正在进行的请求数量
  activeRequests --;

  // 在router获取到route获取，这里获取到的是路由的name，不是路由的path
  // let routerName = router.app._route.name;

  // 业务上的错误
  let res = response.data;
  if (res.code === 401){
    // 增加这一层的判断，这样就不会有多次提示了
    if (loginErr === 0){
      loginErr = true;
      Message.error("未登录或登录已过期");
      // 跳转到首页
      router.push({path: "/"})
    }

    // 如果所有请求都已完成并且没有活动请求，则重置 loginErr
    if (activeRequests === 0) {
      loginErr = false;
    }

  } else if (res.code === 503){
    Message.error("请求过于频繁或重复提交");
  } else if (res.code === 412){
    // Message.error("无访问权限或未授权");
    router.push({path: "/403"})
  } else if (res.code === 408){
    Message.error("请求超时");
  } else if (res.code === 402){
    Message.error("业务错误，拒绝执行");
  } else if (res.code === 403){
    Message.error("非法访问");
  }

  return Promise.resolve(response)

}, error => {
  // 请求完成后减少正在进行的请求数量
  activeRequests --;

  /// 异常情况
  if (error && error.response) {
    switch (error.response.status) {
      case 400:
        error.message = '错误请求'
        break;
      case 401:
        error.message = '未授权，请重新登录'
        break;
      case 403:
        error.message = '拒绝访问'
        router.push({path: "/403"})
        break;
      case 404:
        error.message = '请求错误,未找到该资源'
        // window.location.href = "/404"
        router.push({path: "/404"})
        break;
      case 405:
        error.message = '请求方法未允许'
        break;
      case 408:
        error.message = '请求超时'
        break;
      case 500:
        error.message = '服务器端出错'
        router.push({path: "/500"})
        break;
      case 501:
        error.message = '网络未实现'
        break;
      case 502:
        error.message = '网络错误'
        break;
      case 503:
        error.message = '服务不可用'
        break;
      case 504:
        error.message = '网络超时'
        break;
      case 505:
        error.message = 'http版本不支持该请求'
        break;
      default:
        error.message = `连接错误${error.response.status}`
    }
  } else {
    // 超时处理
    if (JSON.stringify(error).includes('timeout')) {
      Message.error('服务器响应超时，请刷新当前页')
    }
    error.message = '连接服务器失败'
  }

  // Message.error(error.message)
  return Promise.resolve(error.response)
})

//4、--- 导出文件
export default service
