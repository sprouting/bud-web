/**
 * 全局工具类
 */

import CryptoJS from 'crypto-js';
import Moment from 'moment'


/**
 * 检查是否为空
 * @param data 要检查的数据
 * @returns {boolean} null返回true，否则false
 */
export function checkNull (data){
  if (data == null || "" === (data) || "undefined" === (data) || "null" === (data) || " " === (data)) {
    return true;
  } else {
    return false;
  }
}

/**
 * 置空对象的key
 * 注意，可能会导致一些数据污染的问题，使用深拷贝来进行相应的处理可以解决这个问题，
 * 比如：JSON.parse(JSON.stringify(this.dataSourceForm))
 * @param obj
 * @return {*}
 */
export function setObjNull(obj) {
  if (obj == null){
    return obj
  }
  if (Object.prototype.toString.call(obj).trim() === "[object Object]"){
    // 循环对对象每个方法进行置空
    for (let k in obj) {
      // obj[k] 是value , k 是key 键
      obj[k] = null
    }
  }
  return obj
}

/**
 * DES加密，ECB模式
 * @param message 要加密的数据
 * @param key 加密的key，后端提供
 */
export function desEncrypt(message, key) {
  if (checkNull(message)){
    return null
  }
  if (checkNull(key)){
    return null
  }
  // 必须转字符串，否则会报奇怪的错误
  message = message.toString()
  key = key.toString()
  const keyHex = CryptoJS.enc.Utf8.parse(key);
  const option = { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 };
  const encrypted = CryptoJS.DES.encrypt(message, keyHex, option);
  return encrypted.ciphertext.toString();
}

/**
 * DES解密，ECB模式
 * @param message 要解密的数据
 * @param key 解密的key，后端提供
 */
export function decryptByDES(message, key) {
  // 必须转字符串，否则会报奇怪的错误
  message = message.toString()
  key = key.toString()
  const keyHex = CryptoJS.enc.Utf8.parse(key);
  const decrypted = CryptoJS.DES.decrypt({ciphertext: CryptoJS.enc.Hex.parse(message)}, keyHex, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  });
  return decrypted.toString(CryptoJS.enc.Utf8);
}

/**
 * 清理空值，对象
 * @param children
 * @returns {*[]}
 */
export function filterEmpty (children = []) {
  return children.filter(c => c.tag || (c.text && c.text.trim() !== ''))
}

/**
 * 清理掉默认的无效数据
 * @param obj
 */
export function clearInvalid(obj) {
  obj.createTime = null
  obj.updateTime = null
  obj.updateId = null
  obj.createId = null
  obj.delType = null
  return obj
}

// 浏览器对象BOM

// 当前网页地址
export function currentURL() {
  return window.location.href;
}

// 获取滚动条位置
export function getScrollPosition(el = window) {
  return {
    x: el.pageXOffset !== undefined ? el.pageXOffset : el.scrollLeft,
    y: el.pageYOffset !== undefined ? el.pageYOffset : el.scrollTop,
  };
}

// 获取 url 中的参数
export function getURLParameters(url) {
  return url
    .match(/([^?=&]+)(=([^&]*))/g)
    .reduce((a, v) => (
      (a[v.slice(0, v.indexOf("="))] = v.slice(v.indexOf("=") + 1)), a
    ), {});
}

// 页面跳转，是否记录在 history 中
export function redirect(url, asLink = true) {
  asLink ? (window.location.href = url) : window.location.replace(url);
}

// 滚动条回到顶部动画
export function scrollToTop() {
  const scrollTop =
    document.documentElement.scrollTop || document.body.scrollTop;
  if (scrollTop > 0) {
    window.requestAnimationFrame(scrollToTop);
    window.scrollTo(0, c - c / 8);
  } else {
    window.cancelAnimationFrame(scrollToTop);
  }
}

// 复制文本
export function copy(str) {
  const el = document.createElement("textarea");
  el.value = str;
  el.setAttribute("readonly", "");
  el.style.position = "absolute";
  el.style.left = "-9999px";
  el.style.top = "-9999px";
  document.body.appendChild(el);
  const selected =
    document.getSelection().rangeCount > 0 ?
      document.getSelection().getRangeAt(0) :
      false;
  el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
  if (selected) {
    document.getSelection().removeAllRanges();
    document.getSelection().addRange(selected);
  }
}

// 检测设备类型
export function detectDeviceType() {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  ) ?
    "Mobile" :
    "Desktop";
}

// Cookie
// 增
export function setCookie(key, value, expiredays) {
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + expiredays);
  document.cookie = key + "=" + escape(value) + (expiredays == null ? "" : ";expires=" + exdate.toGMTString());
}

// 删
export function delCookie(name) {
  var exp = new Date();
  exp.setTime(exp.getTime() - 1);
  var cval = getCookie(name);
  if (cval != null) {
    document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
  }
}

// 查
export function getCookie(name) {
  var arr,
    reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
  if ((arr = document.cookie.match(reg))) {
    return arr[2];
  } else {
    return null;
  }
}

/**
 * 时间戳（毫秒）转化为标准时间格式
 * @param timeStamp 要转换的时间戳
 * @return {string} 被转换后的值
 */
export function getFullTime(timeStamp) {
  if (timeStamp == null){
    return ""
  }
  const stamp = new Date(timeStamp)
  return Moment(stamp).format('YYYY-MM-DD HH:mm:ss')
}

/**
 * 时间戳（毫秒）转化为标准日期
 * @param timeStamp 要转换的时间戳
 * @return {string} 被转换后的值
 */
export function getFullDate(timeStamp) {
  if (timeStamp == null){
    return ""
  }
  const stamp = new Date(timeStamp)
  return Moment(stamp).format('YYYY-MM-DD')
}

// 日期 Date

// 时间戳转换为时间
// 默认为当前时间转换结果
// isMs 为时间戳是否为毫秒
export function timestampToTime(timestamp = Date.parse(new Date()), isMs = true) {
  const date = new Date(timestamp * (isMs ? 1 : 1000));
  return `${date.getFullYear()}-${
    date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1
  }-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
}

// 文档对象 DOM

/**
 * 固定滚动条
 * 功能描述：一些业务场景，如弹框出现时，需要禁止页面滚动，
 * 这是兼容安卓和 iOS 禁止页面滚动的解决方案
 */
let scrollTop = 0;

export function preventScroll() {
  // 存储当前滚动位置
  scrollTop = window.scrollY;

  // 将可滚动区域固定定位，可滚动区域高度为 0 后就不能滚动了
  document.body.style["overflow-y"] = "hidden";
  document.body.style.position = "fixed";
  document.body.style.width = "100%";
  document.body.style.top = -scrollTop + "px";
  // document.body.style['overscroll-behavior'] = 'none'
}

// 恢复滚动
export function recoverScroll() {
  document.body.style["overflow-y"] = "auto";
  document.body.style.position = "static";
  // document.querySelector('body').style['overscroll-behavior'] = 'none'

  window.scrollTo(0, scrollTop);
}

// 判断当前位置是否为页面底部
export function bottomVisible() {
  return (
    document.documentElement.clientHeight + window.scrollY >=
    (document.documentElement.scrollHeight ||
      document.documentElement.clientHeight)
  );
}

// 判断元素是否在可视范围内
// partiallyVisible 为是否为完全可见
export function elementIsVisibleInViewport(el, partiallyVisible = false) {
  const {
    top,
    left,
    bottom,
    right
  } = el.getBoundingClientRect();

  return partiallyVisible ?
    ((top > 0 && top < innerHeight) ||
      (bottom > 0 && bottom < innerHeight)) &&
    ((left > 0 && left < innerWidth) || (right > 0 && right < innerWidth)) :
    top >= 0 && left >= 0 && bottom <= innerHeight && right <= innerWidth;
}

// 获取元素 css 样式
export function getStyle(el, ruleName) {
  return getComputedStyle(el, null).getPropertyValue(ruleName);
}

// 进入全屏
// launchFullscreen(document.documentElement);
// launchFullscreen(document.getElementById("id")); //某个元素进入全屏
export function launchFullscreen(element) {
  if (element.requestFullscreen) {
    element.requestFullscreen();
  } else if (element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if (element.msRequestFullscreen) {
    element.msRequestFullscreen();
  } else if (element.webkitRequestFullscreen) {
    element.webkitRequestFullScreen();
  }
}

// 退出全屏
export function exitFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.msExitFullscreen) {
    document.msExitFullscreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}

// 全屏事件
document.addEventListener("fullscreenchange", function (e) {
  if (document.fullscreenElement) {
    console.log("进入全屏");
  } else {
    console.log("退出全屏");
  }
});

// 数字 Number
// 数字千分位分割
export function commafy(num) {
  return num.toString().indexOf(".") !== -1 ?
    num.toLocaleString() :
    num.toString().replace(/(\d)(?=(?:\d{3})+$)/g, "$1,");
}

// 生成随机数
export function randomNum(min, max) {
  switch (arguments.length) {
    case 1:
      return parseInt(Math.random() * min + 1, 10);
    case 2:
      return parseInt(Math.random() * (max - min + 1) + min, 10);
    default:
      return 0;
  }
}

// 去除字符串中的html
export function removehtml(str = '') {
  return str.replace(/<[\/\!]*[^<>]*>/ig, '')
}
