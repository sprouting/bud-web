/**
 * 封装全局 axios 的请求
 * */

// 导入实例
import request from './request'

const http = {

  /**
   * 封装get请求
   * @param url 请求地址
   * @param param 请求参数
   */
  get(url, param){
    const config = {
      method: 'get',
      url: url,
      // 是否存在文件, false 不存在文件，true 存在文件
      existingFile: false,
    }

    if (param){
      config.params = param;
    }
    return request(config)
  },

  /**
   * 封装post请求
   * @param url 请求地址
   * @param param 请求参数
   */
  post(url, param){
    const config = {
      method: 'post',
      url: url,
      // 是否存在文件, false 不存在文件，true 存在文件
      existingFile: false,
    }
    if (param){
      config.data = param
    }
    return request(config)
  },

  /**
   * post 提交带文件的上传，同时上传文件和表单数据
   * @param url 请求地址
   * @param param 参数，使用
   *      let fromData = new FormData()
          //  参数为对象时， 使用 JSON.stringify 转字符串
          fromData.append('operation', JSON.stringify( pamram))
          fromData.append('files',uploadFiles[i])
   */
  postFileForm(url, param){
    const config = {
      method: 'post',
      url: url,
      // 是否存在文件, false 不存在文件，true 存在文件
      existingFile: true,
      // data: param
    }
    if (param){
      config.data = param
    }
    return request(config)
  },

  /**
   * 封装 put 请求
   * @param url 请求地址
   * @param param 请求参数
   */
  put(url, param){
    const config = {
      method: 'put',
      url: url,
      // 是否存在文件, false 不存在文件，true 存在文件
      existingFile: false,
    }

    if (param){
      config.params = param
    }
    return request(config)
  },

}

// 暴露出去
export default http
