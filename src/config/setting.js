// 系统全局配置
let ip = "127.0.0.1"
// 测试视频的时候要将这个地址更换为本机IP，否则无法访问以及访问地址异常
// let ip = "192.168.3.39"

// 开发,开发环境指定了IP
module.exports = {
  // 系统名称
  sysName: '花瓣',
  // 资源地址，图片请求地址
  // 需要配置，否则请求的路径没有加端口和项目前缀
  resourceURL: 'http://' + ip + ':8084/upload/asset/',
  // 后台请求地址
  baseURL: 'http://' + ip + ':8086/bud/',
  // socket 请求地址
  socketURL: 'ws://' + ip + ':9001/wsc/',
  // 超时时间 单位是ms
  timeout: 5000,
  // 项目访问端口，如果被占用，会自动选择一个未被占用的
  port: 8087,
  // 项目访问地址，默认 localhost
  // host: 'localhost',
  host: ip,
  // host: '0.0.0.0',
}


// 生产
// module.exports = {
//   // 系统名称
//   sysName: '花瓣',
//   // 资源地址，图片请求地址
//   // 需要配置，否则请求的路径没有加端口和项目前缀
//   resourceURL: './ps/upload/asset/',
//   // 后台请求地址
//   baseURL: './ps',
//   // socket 请求地址
//   socketURL: 'ws://' + ip + ':9001/wsc/',
//   // 超时时间 单位是ms
//   timeout: 5000,
//   // 项目访问端口，如果被占用，会自动选择一个未被占用的
//   port: 8083,
//   // 项目访问地址，默认 localhost
//   // host: 'localhost',
//   host: '0.0.0.0',
// }
