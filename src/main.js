// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// 挂载vuex
import store from "./vuex/store";
// 支持Markdown的富文本
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
// 引入字体
import '@/assets/font/index.css';

// 引入echarts
// import echarts from "echarts";

export default {
  // 组件名称
  name: 'App'
}
Vue.config.productionTip = false

// 挂载 echarts，引入 let myChart = this.$echarts.init(DOM)
// Vue.prototype.$echarts = echarts;

// 使用ElementUI组件
Vue.use(ElementUI);
// 挂载vuex到原型链上，否则直接使用 this.$store.state.navigationList 会报undefined,这可能和vue版本有关系
Vue.prototype.$store= store;
// 挂载支持Markdown的富文本
Vue.use(mavonEditor)

new Vue({
  el: '#app',
  // 挂载路由
  router,
  // 挂载vuex
  store,
  components: { App },
  template: '<App/>'
})

// Vue.prototype.http = http

