import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// 解决Uncaught (in promise) NavigationDuplicated: Avoided redundant navigation to current location:
//使用编程式路由实现跳转的时候，多次执行会抛出NavigationDuplicated的警告错误是因为"vue-router"3版本之后 底层引入了promise,而通过声明式导航没有出现此类问题是因为vue-router底层已经处理好了。
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

export default new Router({
  // 路由
  routes: [
    {
      // 登录页
      path: '/',
      name: 'Login',
      component: () => import('@/components/login/Login2.vue')
    },
    {
      // 登录的参数页面
      path: '/LoginForm',
      name: 'LoginForm',
      component: () => import('@/components/login/LoginForm.vue')
    },
    {
      path: '/register',
      name: 'Register',
      component: () => import('@/components/login/Register.vue'),
    },
    {
      path: '/register2',
      name: 'Register2',
      component: () => import('@/components/login/Register2.vue'),
    },
    {
      path: '/403',
      name: '403',
      component: () => import('@/components/exception/403.vue')
    },
    {
      path: '/404',
      name: '404',
      component: () => import('@/components/exception/404.vue'),
    },
    {
      path: '/500',
      name: '500',
      component: () => import('@/components/exception/500.vue'),
    },
    {
      path: '/toVideoPlaybackNoBorder',
      name: 'toVideoPlaybackNoBorder',
      component: () => import('@/components/toolbox/VideoPlaybackNoBorder.vue'),
    },
    {
      // 系统首页
      path: '/home',
      name: 'Home',
      component: () => import('@/components/home/Home.vue'),
      // 默认页面
      redirect: '/index',
      // 用于父页面进行切换的子路由
      children: [
        {
          // 首页的第一个页面
          path: '/index',
          name: 'Index',
          component: () => import("@/components/home/Index.vue")
        },
        {
          path: '/toDict',
          component: () => import("@/components/sys/DictGroupList.vue")
        },
        {
          path: '/config',
          name: 'config',
          component: () => import('@/components/sys/ConfigList.vue'),
        },
        {
          path: '/toVideoPlayback',
          name: 'toVideoPlayback',
          component: () => import('@/components/toolbox/VideoPlayback.vue'),
        },
        {
          path: '/toDruid',
          name: 'toDruid',
          component: () => import('@/components/sys/Druid.vue'),
        },
      ],
    },


    // {
    //   path: '/home',
    //   name: 'Home',
    //   component: Home,
    //   children:[
    //     {
    //       path: '/initHome',
    //       name: 'InitHome',
    //       component: InitHome
    //     },
    //     {
    //       path: '/dataSourceManagement',
    //       name: 'DataSourceManagement',
    //       component: DataSourceManagement
    //     },
    //   ]
    // },
    // {
    //   path: '/login',
    //   name: 'Login',
    //   component: LoginForm,
    // },
  ]
})
