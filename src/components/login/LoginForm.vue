<template>
  <div id="loginBox">
    <div class="loginTitle">登录</div>
    <el-form :model="loginForm" :rules="rules" ref="loginForm" label-width="100px">
      <el-form-item label="用户名" prop="userName">
        <template slot="label"> <span class="wordClass">用户名</span> </template>
        <el-input class="inputClass" v-model="loginForm.userName"></el-input>
      </el-form-item>
      <el-form-item label="密码" prop="password">
        <template slot="label"> <span class="wordClass">密码</span> </template>
        <el-input class="inputClass" v-model="loginForm.password" show-password></el-input>
      </el-form-item>
      <el-form-item label="验证码" prop="code">
        <template slot="label"> <span class="wordClass">验证码</span> </template>
        <el-input class="inputCode" maxlength="5" v-model="loginForm.code" ></el-input>
        <el-image
          style="width: 120px; height: 40px;margin-left: 10px;float: left;border-radius: 4px"
          :src="captchaImg"
          v-on:click="getVerificationCode"
        ></el-image>
      </el-form-item>
      <el-form-item class="submitClass">
        <el-button type="primary" @click="submitForm('loginForm')" :disabled="submitIsDisabled">立即登录</el-button>
        <el-button type="warning" @click="resetForm('loginForm')" >重置</el-button>
      </el-form-item>
    </el-form>
  </div>
</template>

<script>

import http from '@/utils/http';
import {desEncrypt} from "../../utils/utils";

export default {
  name: "LoginForm",
  data() {
    return {
      captchaImg: '',
      submitIsDisabled: false,
      encryptionKey: '1234',
      loginForm: {
        userName: '',
        password: '',
        code: '',
        keyId: null,
        // 是否强制登录
        force: false,
      },
      // 数据验证
      rules: {
        userName: [
          {required: true, message: '请输入用户名', trigger: 'blur'},
          {min: 1, max: 30, message: '长度在 1 到 30 个字符', trigger: 'blur'}
        ],
        password: [
          {required: true, message: '请输入密码', trigger: 'blur'},
          {min: 1, max: 30, message: '长度在 1 到 30 个字符', trigger: 'blur'}
        ],
        // code: [
        //   {required: true, message: '请输入验证码', trigger: 'blur'},
        //   {min: 4, max: 5, message: '长度在 5 个字符', trigger: 'blur'}
        // ],
      },
    };
  },
  methods: {
    /**
     * 获取验证码
     */
    getVerificationCode(){
      http.get("/open/getVerificationCodeKeyLocal").then(response => {
        let res = response.data;
        if (res.status === 'error'){
          this.$message.error(res.msg);
          return
        }
        this.captchaImg = res.data.images;
        // 加密的key
        this.loginPasswordKey = res.data.encryptionKey;
        // 登录唯一的key
        this.loginForm.keyId = res.data.keyId;

        //存放加密key
        let localStorage = window.localStorage;
        localStorage.loginPasswordKey = this.loginPasswordKey;

      }).catch(error => {
        this.$message.error(error.message);
      })
    },
    /**
     *  登录
     */
    loginPc() {
      // 加密key需要后端提供
      let encryptUser = desEncrypt(this.loginForm.userName, this.loginPasswordKey);
      let encryptPassword = desEncrypt(this.loginForm.password, this.loginPasswordKey);

      http.post("/open/loginPcLocal", {
        // 请求参数
        userName: encryptUser,
        password: encryptPassword,
        code: this.loginForm.code,
        keyId: this.loginForm.keyId,
        force: this.loginForm.force

      }).then(response => {
        let res = response.data;
        if (!res.success){
          this.$message.error(res.msg);
          return
        }
        console.log(res)
        if (res.code === 101){
          // 强制登录
          this.isForce(res.msg)
          return;
        }
        // 登录完成
        let localStorage = window.localStorage;
        localStorage.token = res.tokenValue;
        localStorage.user = JSON.stringify(res.user);
        localStorage.authTreeList = JSON.stringify(res.authTreeList);
        localStorage.roleList = JSON.stringify(res.roleList);

        this.$router.push({path: "/home"})

      }).catch(error => {
        console.log(error)
        this.$message.error(error.message);
      })
    },
    /**
     * 是否强制保存
     */
    isForce(returnMsg) {
      this.$confirm(returnMsg, '提示', {
        distinguishCancelAndClose: true,
        confirmButtonText: '强制登录',
        cancelButtonText: '取消登录'
      })
        .then(() => {
          this.loginForm.force = true
          this.loginPc()
        })
        .catch(action => {
          this.loginForm.force = false
        });
    },
    /**
     * 表单提交
     * @param formName
     */
    submitForm(formName) {
      // 禁止重复提交
      this.submitIsDisabled = true;
      this.$refs[formName].validate((valid) => {
        if (valid) {
          this.loginPc();

        } else {
          this.$message.error('参数填写不合法');
          return false;
        }
      });

      // 重新刷新验证码
      this.getVerificationCode();
      this.submitIsDisabled = false;
    },
    /**
     * 重置表单
     * @param formName
     */
    resetForm(formName) {
      this.$refs[formName].resetFields();
    },
  },
  // 钩子函数，一般在初始化页面完成后，再对dom节点进行相关操作
  mounted(){
    this.getVerificationCode();
  }
}
</script>

<style scoped>
#loginBox {
  /*background-color: black;*/
  /*透明*/
  /*opacity: 0.4*/
  background-color: rgba(0, 0, 0, 0.3);
  /*background-color: rgba(255, 255, 255, 0.2);*/ /* 半透明背景,这种太深了，在这里有点不合适 */
  backdrop-filter: blur(10px); /* 背景模糊 */
  -webkit-backdrop-filter: blur(10px);
  /*filter:progid: DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);低版本ie透明写法*/
  /*设置圆角*/
  border-radius: 25px;
}

.wordClass {
  /*color: red;*/
  color: #ddd;
}


.inputClass {
  width: 80%;
}

.loginTitle {
  text-align: center;
  margin-top: 10px;
  margin-bottom: 10px;
  /*color: skyblue;*/
  color: #ffffff;
  font-size: 24px;
  font-family: "San Francisco Pro","Roboto","Helvetica","PingFang SC","苹方","Microsoft Yahei","微软雅黑","Source Han Sans CN","思源黑体","SimHei","黑体","SimSun","宋体",sans-serif;
}

.inputCode {
  width: 150px;
  float: left;
}

.submitClass {
  text-align: left;
}

/*毛玻璃效果*/
.blur-element {
  background-color: rgba(255, 255, 255, 0.2); /* 半透明背景 */
  backdrop-filter: blur(10px); /* 背景模糊 */
  -webkit-backdrop-filter: blur(10px); /* 兼容老版本 Safari */
}

</style>
